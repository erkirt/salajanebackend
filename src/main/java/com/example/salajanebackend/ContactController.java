package com.example.salajanebackend;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping(path = "contact") //et ei oleks lihtsalt 8080
public class ContactController {


    private final ContactRepository contactRepository;

    @Autowired
    public ContactController(ContactRepository contactRepository) {
        this.contactRepository = contactRepository;
    }

    @GetMapping("/all")
    public ResponseEntity<List<Contact>> getContact(){
        List<Contact> contact =  contactRepository.findAll();
        return new ResponseEntity<>(contact, HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<Optional<Contact>> getContactById(@PathVariable("id") Long id){
        Optional<Contact> contact =  contactRepository.findById(id);
        return new ResponseEntity<>(contact, HttpStatus.OK);
    }
    @GetMapping("/details/{id}")
    public ResponseEntity<List<Contact>> getContactDetailsById(@PathVariable("id") Long id){
        List<Contact> contact =  contactRepository.findAllById(Collections.singleton(id));
        return new ResponseEntity<>(contact, HttpStatus.OK);
    }

    @GetMapping("/search/{searchTerm}")
    public ResponseEntity<List<Contact>> getContactsBySearch(@PathVariable("searchTerm") String searchTerm){
        String[] searchTermSplit = searchTerm.split(" ");
        List<Contact> contacts;
        if (searchTermSplit.length > 1){
            contacts =
                    contactRepository.findContactsByFirstNameIsLikeAndLastNameIsLikeAllIgnoreCase(searchTermSplit[0], searchTermSplit[searchTermSplit.length - 1]);
        }
        else{
            contacts =
                    contactRepository.findContactsByFirstNameContainsOrLastNameContainsAllIgnoreCase(searchTerm, searchTerm);
        }

        return new ResponseEntity<>(contacts, HttpStatus.OK);
    }


    @PostMapping("/add")
    ResponseEntity<Contact> addContact(@RequestBody Contact contact){
        Contact newContact = contactRepository.save(contact);
        return new ResponseEntity<>(newContact, HttpStatus.CREATED);
    }
    @PutMapping("/{id}")
    public ResponseEntity<Contact> updateContactById(@PathVariable("id") Long id, @RequestBody Contact contact){
        Contact updateableContact =  contactRepository.findById(id).orElseThrow(() ->
                new RuntimeException("Contact with id "+ id + " could not be found"));


        updateableContact.setFirstName(contact.getFirstName());
        updateableContact.setLastName(contact.getLastName());
        updateableContact.setCodeName(contact.getCodeName());
        updateableContact.setPhoneNumber(contact.getPhoneNumber());

        Contact updatedContact = contactRepository.save(updateableContact);

        return new ResponseEntity<>(updatedContact, HttpStatus.OK);
    }
    @DeleteMapping("/{id}")
    public ResponseEntity<HttpStatus> deleteContact(@RequestBody Long id){
        try {
            contactRepository.deleteById(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
