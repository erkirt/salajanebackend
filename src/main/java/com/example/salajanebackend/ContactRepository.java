package com.example.salajanebackend;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ContactRepository  extends JpaRepository<Contact, Long> {
    List<Contact> findContactsByFirstNameIsLikeAndLastNameIsLikeAllIgnoreCase(String inputFirstName, String inputLastName);
    List<Contact> findContactsByFirstNameContainsOrLastNameContainsAllIgnoreCase(String inputFirstName, String inputLastName);

}
