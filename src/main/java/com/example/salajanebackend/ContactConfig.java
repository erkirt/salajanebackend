package com.example.salajanebackend;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;

import java.util.List;

@SpringBootApplication
public class ContactConfig {

    public static void main(String[] args) {
        SpringApplication.run(ContactConfig.class, args);
    }


    @Bean
    public CorsFilter corsFilter() {
        CorsConfiguration configuration = new CorsConfiguration();
        configuration.setAllowCredentials(true);
        configuration.setAllowedOrigins(List.of("http://localhost:4200"));
        configuration.setAllowedHeaders(List.of("Origins", "Access-Control-Allow-Origin", "Content-Type", "Accept",
                "Authorization", "Origin, Accept", "X-Requested-With", "Access-Control-Request-Method",
                "Access-Control-Request-Headers"));
        configuration.setExposedHeaders(List.of("Origins", "Content-Type", "Accept",
                "Authorization", "Access-Control-Allow-Origin", "Access-Control-Allow-Credentials"));
        configuration.setAllowedMethods(List.of("GET", "POST", "PUT", "DELETE", "OPTIONS"));

        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        source.registerCorsConfiguration("/**", configuration);
        return new CorsFilter(source);
    }

    @Bean
    CommandLineRunner commandLineRunner(ContactRepository repository){

        //ei lase ilma id-ta buildida

        return args -> {
            Contact ergo = new Contact(
                    null,
                    "Ergo",
                    "Kirt",
                    "Sass",
                    "58855203"
            );

            Contact vega = new Contact(
                    null,
                    "Agathe",
                    "Vega",
                    "Natalija",
                    "53359933"
            );
            Contact jaanus = new Contact(
                    null,
                    "Jaanus",
                    "Peetrus",
                    "Ants",
                    "5894561"
            );
            repository.saveAll(List.of(ergo, vega, jaanus));
        };
    }
}
